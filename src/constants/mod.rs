pub mod response {
    pub const OFFSET_PTR : &'static str = "/offset";
    pub const LIMIT_PTR  : &'static str = "/limit";
    pub const OFFSET     : &'static str = "offset";
    pub const LIMIT      : &'static str = "limit";
    pub const META       : &'static str = "meta";
    pub const NEXT       : &'static str = "next";
    pub const COUNT      : &'static str = "count";
    pub const PREVIOUS   : &'static str = "previous";
    pub const DATA       : &'static str = "data";
    pub const URI        : &'static str = "uri";
}
