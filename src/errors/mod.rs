use std::fmt;
pub use std::error::Error;

#[derive(Debug)]
pub struct UnauthorizedError;

impl Error for UnauthorizedError {
    fn description(&self) -> &str {
        return "UnauthorizedError";
    }
}

impl fmt::Display for UnauthorizedError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.description().fmt(formatter)
    }
}

#[derive(Debug)]
pub struct NotFoundError;

impl Error for NotFoundError {
    fn description(&self) -> &str {
        "Not Found"
    }
}

impl fmt::Display for NotFoundError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        self.description().fmt(formatter)
    }
}
