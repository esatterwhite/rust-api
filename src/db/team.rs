extern crate uuid;
extern crate time;
extern crate jsonway;
extern crate postgres;
extern crate chrono;

use self::chrono::NaiveDate;
use self::postgres::Connection;
use self::time::Timespec;
use self::uuid::Uuid;

const ID         : &'static str = "auth_user_id";
const FIRST_NAME : &'static str = "first_name";
const LAST_NAME  : &'static str = "last_name";
const CREATED    : &'static str = "created_at";
const NAME       : &'static str = "name";
const BIRTHDATE  : &'static str = "birthdate";



const LIST_TEAM_SQL: &'static str =  "
SELECT 
    league_team.* 
    , (select count (league_team_id)::integer from league_team) as total
FROM league_team 
LIMIT $1 OFFSET $2
";

const LIST_TEAM_ROSTER_SQL: &'static str = "
SELECT
    league_team_player.year as year,
    position,
    first_name,
    last_name,
    (
      SELECT COUNT(league_team_player_id)
      FROM league_team_player
      WHERE league_team_player.year = $1
      AND league_team_player.league_team_id = $2
    )::integer as total
FROM
    league_team_player
LEFT JOIN 
    auth_user on auth_user.auth_user_id = league_team_player.auth_user_id
WHERE 
    league_team_player.year = $1
AND
    league_team_player.league_team_id = $2
LIMIT $3 OFFSET $4
";

const TEAM_BY_ID: &'static str = "
SELECT * from league_team WHERE league_team_id = $1
";

///
/// Team Model
///
pub struct Team {
    pub name           : String,
    pub nickname       : String,
    pub city           : String,
    pub state          : String,
    pub league_team_id : uuid::Uuid,
    pub created_at     : time::Timespec
}

pub struct TeamPlayer {
   pub year: i32,
   pub position: String,
   pub first_name: String,
   pub last_name: String
}

impl Team {
    pub fn latest(conn: &Connection, limit: i64, offset: i64) -> (Vec<Team>, i32) {
        let rows = conn.query(LIST_TEAM_SQL,&[&limit, &offset]).unwrap();
        let count: i32 = rows.get(0).get("total");
        let mut teams = vec![];
        for row in rows.iter() {
            let team = Team {
                name           : row.get("name"),
                nickname       : row.get("nickname"),
                city           : row.get("city"),
                league_team_id : row.get("league_team_id"),
                created_at     : row.get("created_at"),
                state          : row.get("state")
            };
            teams.push(team); 
        }
        (teams, count)
    } 
    pub fn get(conn: &Connection, team_id: Uuid) -> Team {
        let rows = conn.query(TEAM_BY_ID, &[&team_id]).unwrap();
        let t = rows.get(0);

        Team {
            name           : t.get("name")
          , nickname       : t.get("nickname")
          , city           : t.get("city")
          , state          : t.get("state")
          , league_team_id : t.get("league_team_id")
          , created_at     : t.get("created_at")
        }
    }
    
    pub fn roster(conn: &Connection) -> Vec<Team> {
        vec![]
    }

    pub fn roster_by_year(conn: &Connection, team_id: Uuid, year: i64, limit: i64, offset: i64) -> (Vec<TeamPlayer>, i32) {
        let mut players = vec![];
        let _yr: i32 = year as i32;
        let rows        = conn.query(LIST_TEAM_ROSTER_SQL, &[&_yr, &team_id, &limit, &offset]).unwrap();
        let count: i32  = if rows.len() > 0 {
            rows.get(0).get("total")
        } else { 0 };

        for row in rows.iter() {
            let player = TeamPlayer {
                first_name : row.get(FIRST_NAME),
                last_name  : row.get(LAST_NAME),
                position: row.get("position"),
                year: row.get("year")
            };
            players.push(player);
        }

        (players, count)
    }
}
