/// TODO - Break this apart some
extern crate uuid;
extern crate time;
extern crate postgres;


use self::postgres::Connection;
use self::time::Timespec;
use self::uuid::Uuid;

pub use super::league::League;
pub use super::team::Team;

const TEAMS_BY_USER: &'static str = "
SELECT 
  league_team_player.year
, auth_user.last_name
, auth_user.first_name
, position
, (
    SELECT count(league_team_player_id)::integer
    FROM league_team_player
    where auth_user_id = $1
    ) as total
, league_team.name as team
, league_team.nickname
, league_team.city
, league_team_player.position
, league_team_player.auth_user_id::varchar
, league_team_player.league_team_id::varchar
FROM 
    league_team_player
INNER JOIN 
    auth_user on auth_user.auth_user_id = league_team_player.auth_user_id
INNER JOIN 
    league_team on league_team_player.league_team_id = league_team.league_team_id
WHERE 
    league_team_player.auth_user_id = $1
ORDER BY year ASC
LIMIT $2 OFFSET $3
";

const TEAMS_BY_YEAR: &'static str = "
SELECT 
	auth_user.first_name
    , auth_user.last_name
	, league_team_player.position
	, extract( year from league_team_player.year ) as year
	, league_team.name as team
	, league_team.nickname
	, league_team.city
    , league_team_player.auth_user_id
    , league_team_player.league_team_id
FROM auth_user
INNER JOIN league_team_player on (league_team_player.auth_user_id = auth_user.auth_user_id)
INNER JOIN league_team on (league_team_player.league_team_id = league_team.league_team_id)
WHERE auth_user.auth_user_id = $1
ORDER BY year DESC;
";

const LIST_USER_SQL: &'static str = "
SELECT 
    * 
  , (select count(auth_user_id) from auth_user)::integer as total
FROM 
    auth_user 
LIMIT $1 OFFSET $2
";

///
/// User Model
///
pub struct User {
    pub auth_user_id : uuid::Uuid,
    pub first_name   : String,
    pub last_name    : String,
    pub created_at   : time::Timespec,
}

const ID         : &'static str = "auth_user_id";
const FIRST_NAME : &'static str = "first_name";
const LAST_NAME  : &'static str = "last_name";
const CREATED    : &'static str = "created_at";
const NAME       : &'static str = "name";
const BIRTHDATE  : &'static str = "birthdate";

pub struct UserTeam {
    pub year           : i32,
    pub first_name     : String,
    pub last_name      : String,
    pub team           : String,
    pub city           : String,
    pub nickname       : String,
    pub position       : String,
    pub league_team_id : String,
    pub auth_user_id   : String,
}

impl User {

    pub fn new(id: Uuid, first_name: String, last_name: String, created_at: Timespec ) -> User {
        User {
            auth_user_id : id,
            last_name    : last_name,
            first_name  : first_name,
            created_at   : created_at,
        }
    }

    pub fn get(conn: &Connection, user_id: Uuid) -> User {
        let rows = conn.query("SELECT * from auth_user where auth_user_id = $1", &[&user_id]).unwrap();
        let u = rows.get(0);

        User::new(
            u.get(ID),
            u.get(FIRST_NAME),
            u.get(LAST_NAME),
            u.get(CREATED),
        )
    }

    pub fn latest(conn: &Connection, limit: i64, offset: i64) -> (Vec<User>, i32) {
        let mut users = vec![];
        let rows = conn.query(LIST_USER_SQL,  &[&limit, &offset]).unwrap();
        let count: i32 = if rows.len() > 0 { rows.get(0).get("total") } else { 0 };
        for row in rows.iter() {
            let user = User::new(
                row.get(ID),
                row.get(FIRST_NAME),
                row.get(LAST_NAME),
                row.get(CREATED),
            );

            users.push( user );
        }

        (users,count)
    }
    pub fn teams(conn: &Connection, user_id: Uuid, limit: i64, offset: i64) -> (Vec<UserTeam>, i32) {
        let rows = conn.query(TEAMS_BY_USER, &[&user_id, &limit, &offset]).unwrap();

        let count: i32 = if rows.len() > 0 { rows.get(0).get("total") } else { 0 };
        let mut teams = vec![];
        for row in rows.iter() {
            let team = UserTeam {
                year           : row.get("year"),
                first_name     : row.get(FIRST_NAME),
                last_name      : row.get(LAST_NAME),
                team           : row.get("team"),
                city           : row.get("city"),
                nickname       : row.get("nickname"),
                position       : row.get("position"),
                auth_user_id   : row.get("auth_user_id"),
                league_team_id : row.get("league_team_id"),
            };

            teams.push( team );
        }
        (teams, count)
    }

    pub fn teams_by_year(conn: &Connection, user_id: Uuid, year: i32) -> Vec<UserTeam> {
        let rows = conn.query( TEAMS_BY_YEAR, &[&year] ).unwrap();
        let mut users = vec![];

        for row in rows.iter() {
            let year = row.get("year");
            let user = UserTeam{
                year           : year,
                first_name     : row.get(FIRST_NAME),
                last_name      : row.get(LAST_NAME),
                team           : row.get("team"),
                city           : row.get("city"),
                nickname       : row.get("nickname"),
                position       : row.get("position"),
                auth_user_id   : row.get("auth_user_id"),
                league_team_id : row.get("league_team_id"),
            };

            users.push(user);
        }

        users
    }
}


///
/// Game Model
/// -- needs work...
///
pub struct Game {
    pub league_game_id : uuid::Uuid,
    pub created_at     : time::Timespec,
    pub home_team      : uuid::Uuid,
    pub away_team      : uuid::Uuid,
    pub score          : i32
}

impl Game {
    pub fn new(home: uuid::Uuid, away: uuid::Uuid) -> Self {
        Game {
            league_game_id : uuid::Uuid::new_v4(),
            home_team      : home,
            away_team      : away,
            score          : 0,
            created_at     : time::get_time(),
        }
    }

    pub fn latest(conn: &Connection) -> Vec<Game> {
        let rows = conn.query("
                   SELECT * FROM league_game
                   LIMIT 25
                   ", &[]).unwrap();

        let mut games = vec![];
        for row in rows.iter() {
            let game = Game {
              league_game_id : row.get("league_game_id")
            , home_team      : row.get("home_team")
            , away_team      : row.get("away_team")
            , score          : row.get("score")
            , created_at     : row.get("created_at")
            };

            games.push( game );
        }

        games
    }

}
