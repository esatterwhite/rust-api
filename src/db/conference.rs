extern crate time;

use self::time::Timespec;

pub struct Conference {
    pub league_conference_id: String
    ,pub name: String
    ,pub created_at: Timespec
}
