extern crate typemap;
extern crate rustless;
extern crate r2d2;
extern crate postgres;
use self::rustless::Extensible;

mod league;
mod team;
mod conference;
pub mod models;

/// Postgres sub module
mod pg {
    extern crate r2d2;
    extern crate r2d2_postgres;
    pub use self::r2d2::{Pool, PooledConnection};
    pub use self::r2d2_postgres::PostgresConnectionManager as PGConnection;
    pub use self::r2d2::Config;
    pub type PostgresPool = Pool<PGConnection>;
    pub type PostgresPooledConnection<'a> = PooledConnection<PGConnection,>;
    pub use self::r2d2_postgres::TlsMode;
}

pub fn setup(cn_str: &str, pool_size: u32) -> pg::PostgresPool {
    let manager = pg::PGConnection::new(cn_str, pg::TlsMode::None).unwrap();
    let config = pg::Config::builder()
        .pool_size(pool_size)
        .build();

    pg::Pool::new(config, manager).unwrap()
}

/// Database Extension Trait
pub trait DatabaseExt: rustless::Extensible {
	fn db(&self) -> pg::PostgresPooledConnection;
}

/// AppDb
pub struct AppDb;
impl typemap::Key for AppDb {
	type Value = pg::PostgresPool;
}

impl DatabaseExt for rustless::Application {
	fn db(&self) -> pg::PostgresPooledConnection {
		self.ext().get::<AppDb>().unwrap().get().unwrap()
	}
}
