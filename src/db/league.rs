extern crate uuid;
extern crate time;
extern crate jsonway;
extern crate postgres;
extern crate chrono;
extern crate rustless;

use self::uuid::Uuid;
use self::rustless::JsonValue;
use self::chrono::NaiveDate;
use self::time::Timespec;
use self::postgres::Connection;
use super::conference::Conference;

pub struct League {
    pub league_id: String,
    pub name: String,
    pub created_at: Timespec,
    pub founded_at: NaiveDate
}


const LIST_LEAGUES: &'static str = "
SELECT 
    league_id::varchar,
    created_at,
    founded_at,
    name,
    ( select COUNT(league_id)::integer from league )as total
FROM league
ORDER BY founded_at DESC
LIMIT $1 OFFSET $2
";

const LIST_CONFERENCE_BY_LEAGUE: &'static str = "
SELECT 
    lc.league_conference_id::varchar
  , lc.name
  , lc.created_at
  , lcy.year
  , league.name
  , ( 
    SELECT COUNT(*)
    FROM league_conference_year 
    WHERE league_id = $1
    )::integer as total
FROM
    league_conference_year as lcy
INNER JOIN
    league_conference lc on lc.league_conference_id = lcy.league_conference_id
INNER JOIN
    league on league.league_id = lcy.league_id
WHERE 
    lcy.league_id = $1
ORDER BY 
    lcy.year DESC
LIMIT $2 OFFSET $3
";

const LIST_CONFERENCE_BY_LEAGUE_YEAR: &'static str = "
SELECT 
    lc.league_conference_id::varchar
  , lc.name
  , lc.created_at
  , lcy.year
  , league.name
  , ( 
    SELECT COUNT(*)
    FROM league_conference_year 
    WHERE league_id = $1 AND year = $4
    )::integer as total
FROM
    league_conference_year as lcy
INNER JOIN
    league_conference lc on lc.league_conference_id = lcy.league_conference_id
INNER JOIN
    league on league.league_id = lcy.league_id
WHERE 
    lcy.league_id = $1
AND
    lcy.year = $4
ORDER BY 
    lcy.year DESC
LIMIT $2 OFFSET $3
";


const LIST_COFERENCE_BY_TEAM: &'static str = "
SELECT 
	ltc.league_team_id
	,lcc.year
	,league_conference.name as conference
	,league_team.*
FROM 
	league_team_conference ltc
INNER JOIN 
	league_team on league_team.league_team_id = ltc.league_team_id
inner JOIN
	league_conference_year lcc on lcc.league_conference_year_id = ltc.league_conference_year_id
INNER JOIN
	league_conference on league_conference.league_conference_id = lcc.league_conference_id
WHERE 
	ltc.league_team_id = $1
AND
	year = $2
";

const GET_LEAGUE: &'static str = "
SELECT 
    league_id::varchar,
    created_at,
    founded_at,
    name,
    ( select COUNT(league_id)::integer from league )as total
FROM league
WHERE league_id = $1
";

impl League {
    pub fn latest(conn: &Connection, limit: i64, offset: i64) -> (Vec<League>, i32) {
        let rows = conn.query(LIST_LEAGUES, &[&limit, &offset]).unwrap();
        let total: i32 = rows.get(0).get(4);
        let mut leagues: Vec<League> = vec![];

        for row in rows.iter() {
            let league = League {
                league_id: row.get("league_id")
              , name: row.get("name")
              , created_at: row.get("created_at")
              , founded_at: row.get("founded_at")
            };
            leagues.push( league );
        }

        (leagues, total)
    }

    pub fn get(conn: &Connection, league_id: Uuid) -> Option<League> {
        let rows = conn.query(GET_LEAGUE, &[&league_id]).unwrap();
        if rows.len() == 0 {
            return None
        } else {
            let t = rows.get(0);

            let l = League {
              league_id: t.get("league_id"),
              name: t.get("name"),
              created_at: t.get("created_at"),
              founded_at: t.get("founded_at")
            };

            Some(l)
        }


    }
    
    pub fn conference_by_league(conn: &Connection, league_id: &Uuid, limit: &i64, offset: &i64, year: Option<&JsonValue>) -> (Vec<Conference>, i32) {
        let rows = if year.is_none() {
            conn.query(
              LIST_CONFERENCE_BY_LEAGUE
            , &[league_id, limit, offset]
            ).unwrap()
        } else {
            let y = year.unwrap().as_i64().unwrap() as i32;
            conn.query(
               LIST_CONFERENCE_BY_LEAGUE_YEAR
            , &[league_id, limit, offset, &y]
            ).unwrap()
        };

        let total: i32 = if rows.len() > 0 { rows.get(0).get("total") } else { 0 };
        let mut confs: Vec<Conference> = vec![];
        for row in rows.iter() {
            let con = Conference {
                league_conference_id: row.get("league_conference_id"),
                name: row.get("name"),
                created_at: row.get("created_at"),
            };
            confs.push(con);
        }
        (confs, total)
    }
}
