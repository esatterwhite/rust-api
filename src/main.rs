#[macro_use]
extern crate rustless;
extern crate iron;
extern crate hyper;
extern crate valico;

mod errors;
mod api;
mod db;
mod constants;

use std::env;
use rustless::{Application};
use rustless::batteries::swagger;


fn setup_db(app: &mut Application) {
    let user = match env::var( "DB_USER" ) {
        Ok(val) => val,
        _ => String::from("admin")
    };

    let pass = match env::var( "DB_PASS" ) {
        Ok(val) => val,
        _ => String::from("abc123")
    };

    let name = match env::var( "DB_NAME" ) {
        Ok(val) => val,
        _ => String::from("application")
    };

    let host = match env::var( "DB_HOST" ) {
        Ok(val) => val,
        _ => String::from("localhost")
    };

    let port = match env::var( "DB_PORT" ) {
        Ok(val) => val,
        _ => String::from("5432")
    };

    let database_url = match env::var( "DATABASE_URL" ) {
        Ok(val) => val,
        _ => String::new()
    };

    let conn_str = if database_url.len() > 0 {
        database_url
    } else {
        format!(
          "postgres://{user}:{pass}@{host}:{port}/{name}"
          , user=user
          , pass=pass
          , host=host
          , name=name
          , port=port
        )
    };

    let pool = db::setup(&conn_str[..], 5);
    app.ext.insert::<db::AppDb>(pool);
}

fn main (){
    let mut app = Application::new(api::root());
    setup_db(&mut app);
    let port = match env::var("PORT") {
        Ok(val) => val,
        _ => String::from("4000")
    };
    swagger::enable(&mut app, swagger::Spec {
        info: swagger::Info {
            title: "Sports Ball API".to_string(),
            description: Some("An expoloritory historical and statistacl api".to_string()),
            contact: Some(swagger::Contact {
                name: "Eric Satterwhite".to_string(),
                url: Some("http://codedpendant.net".to_string()),
                ..std::default::Default::default()
            }),
            license: Some(swagger::License {
                name: "MIT".to_string(),
                url: "http://opensource.org/licenses/MIT".to_string()
            }),
            ..std::default::Default::default()
        },
        ..std::default::Default::default()
    });
    println!("server running {}", port);
    iron::Iron::new(app).http(format!("0.0.0.0:{}",port).as_str()).unwrap();
}
