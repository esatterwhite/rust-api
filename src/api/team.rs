extern crate uuid;
extern crate time;
extern crate rustless;
extern crate valico;
extern crate jsonway;

use self::uuid::Uuid;
use constants::response;
use valico::json_dsl;
use rustless::framework::endpoint::EndpointHandlerPresent;
use rustless::{Endpoint, Nesting, Namespace};
use db::DatabaseExt;
use db::models::Team;
use api::pagination;

const URL: &'static str ="/api/v1team";

pub fn api(path: &str) -> Namespace {
    Namespace::build(path, |team_api| {
        team_api.get("", get_list);
        team_api.get(":id", get_detail);
        team_api.get(":id/roster/:year", roster_by_year)
    })
}


fn get_list<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
    endpoint.desc("List all teams");
    endpoint.params(|params| {
        params.opt(response::LIMIT, |lmt| {
            lmt.coerce(json_dsl::i64());
            lmt.desc("Number of results to return");
            lmt.default(25)
        });

        params.opt(response::OFFSET, |offset| {
            offset.desc("The number of records to skip");
            offset.default(0);
            offset.coerce(json_dsl::i64())
        })
    });

    endpoint.handle(|client, params| {
        let conn     = client.app.db();
        let limit    = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
        let offset   = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
        let results  = Team::latest(&*conn, limit, offset);
        let packet   = jsonway::object(|json| {
            json.object("meta", |meta| {
                let total = results.1 as i64;
                let n = pagination::next(URL, &limit, &offset, &total );
                let p = pagination::prev(URL, &limit, &offset, &total );

                meta.set(response::COUNT, results.1);
                meta.set(response::LIMIT, limit);
                meta.set(response::URI, URL);
                meta.set(response::NEXT, n);
                meta.set(response::PREVIOUS, p);
            });
            json.array("data", |data| {
                data.objects(&mut results.0.iter(), |team, obj| {                
                    obj.set("id", team.league_team_id.to_string());
                    obj.set("name", team.name.to_string());
                    obj.set("nickname", team.nickname.to_string());
                    obj.set("city", team.city.to_string());
                    obj.set("state", team.state.to_string());
                    obj.set("created_at", time::at_utc(team.created_at.clone()).rfc3339().to_string())
                })
            });
        });
        client.json(&packet.unwrap())
    })
}

fn get_detail<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
    endpoint.handle(|client, params| {
        let conn = client.app.db();
        let team_id: Uuid = Uuid::parse_str(params.pointer("/id").unwrap().as_str().unwrap()).unwrap();
        let team  = Team::get(&*conn, team_id);

        let response = jsonway::object(|obj| {
            obj.set("auth_user_id", team.league_team_id.to_string());
            obj.set("name", team.name.to_string());
            obj.set("city", team.city.to_string());
            obj.set("state", team.state.to_string());
            obj.set("nickname", team.nickname.to_string());
            obj.set("created_at", time::at_utc(team.created_at.clone()).rfc3339().to_string());
        });
        client.json(&response.unwrap())
    })
}

fn roster_by_year<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
    endpoint.desc("List Team Roster by year");
    endpoint.params(|params| {
        params.opt(response::LIMIT, |lmt| {
            lmt.coerce(json_dsl::i64());
            lmt.desc("Number of results to return");
            lmt.default(25)
        });

        params.opt(response::OFFSET, |offset| {
            offset.desc("The number of records to skip");
            offset.default(0);
            offset.coerce(json_dsl::i64())
        });

        params.req("year", |year| {
            year.desc("The year to to look up: 2000");
            year.coerce(json_dsl::i64())
        });
    });

    endpoint.handle(|client, params| {
        let conn          = client.app.db();
        let limit         = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
        let offset        = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
        let year          = params.pointer("/year").unwrap().as_i64().unwrap();
        let next          = offset + limit;
        let previous      = if offset == 0 { 0 } else {offset - limit};

        let team_id: Uuid = Uuid::parse_str(params.pointer("/id").unwrap().as_str().unwrap()).unwrap();
        let results       = Team::roster_by_year(&*conn, team_id, year, limit, offset);

        let packet = jsonway::object(|json| {
            json.object(response::META, |meta| {
                meta.set(response::NEXT, format!("/api/v1/team?limit={limit}&offset={next}", next=&next, limit=&limit)); 
                meta.set(response::PREVIOUS, format!("/api/v1/team?limit={limit}&offset={previous}", previous=&previous, limit=&limit));
                meta.set(response::COUNT, results.1);
                meta.set(response::LIMIT, limit);
            });
            
            json.array(response::DATA, |data| {
                data.objects(&mut results.0.iter(), |player, obj| {
                    obj.set("position", player.position.to_string());
                    obj.set("first_name", player.first_name.to_string());
                    obj.set("last_name", player.last_name.to_string());
                    obj.set("year", player.year.to_string());
                })
            })
        });

        client.json(&packet.unwrap())
    })
}

