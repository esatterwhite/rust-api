extern crate rustless;
use rustless::{Nesting, Namespace};

pub fn api(path: &str) -> Namespace {
    Namespace::build(path, |leagues| {
        leagues.get("", routes::get_list);
        leagues.get(":id", routes::get_detail);
        leagues.get(":id/conference", routes::get_conference);
    })
}

mod routes {
    extern crate jsonway;
    extern crate rustless;
    extern crate valico;
    extern crate chrono;
    extern crate time;
    extern crate uuid;

    use errors;
    use self::uuid::Uuid;
    use self::valico::json_dsl;
    use rustless::Endpoint;
    use api::pagination;
    use rustless::framework::endpoint::EndpointHandlerPresent;
    use db::models::League;
    use db::DatabaseExt;
    use constants::response;

    const PREFIX: &'static str = "/api/v1/league";

    pub fn get_list<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.desc("List all Leagues");

        endpoint.params(|params| {
            params.opt(response::LIMIT, |lmt| {
                lmt.coerce(json_dsl::i64());
                lmt.desc("Number of results to return");
                lmt.default(25)
            });

            params.opt(response::OFFSET, |offset| {
                offset.desc("The number of records to skip");
                offset.default(0);
                offset.coerce(json_dsl::i64())
            })
        });

        endpoint.handle(|client, params| {
            let conn     = client.app.db();
            let limit    = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
            let offset   = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
            let results  = League::latest(&*conn, limit, offset);
            let packet   = jsonway::object(|json| {
                json.object(response::META, |meta| {
                    let total = results.1 as i64;
                    let n = pagination::next(PREFIX, &limit, &offset, &total );
                    let p = pagination::prev(PREFIX, &limit, &offset, &total );

                    meta.set(response::COUNT, results.1);
                    meta.set(response::LIMIT, limit);
                    meta.set(response::URI, PREFIX);
                    meta.set(response::NEXT, n);
                    meta.set(response::PREVIOUS, p);
                });

                json.array(response::DATA, |data| {
                    data.objects(&mut results.0.iter(), |league, obj| {
                        obj.set("league_id", &league.league_id);
                        obj.set("name", &league.name);
                        obj.set("created_at", time::at_utc(league.created_at.clone()).rfc3339().to_string());
                        obj.set("founded_at",league.founded_at.format("%Y-%m-%d").to_string());
                    })
                })
            });
            client.json(&packet.unwrap())
        })
    }

    pub fn get_detail<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.desc("Get an individual league");
        endpoint.handle(|client, params| {
            let conn = client.app.db();
            let league_id: Uuid = Uuid::parse_str(params.pointer("/id").unwrap().as_str().unwrap()).unwrap();
            let opt = League::get(&*conn, league_id);

            if opt.is_none() {
                return client.error(errors::NotFoundError) 
            }

            let league = opt.unwrap();
            let packet = jsonway::object(|json| {
               let created = time::at_utc(league.created_at.clone()).rfc3339().to_string();
               json.set("league_id", league.league_id.to_string());
               json.set("name", league.name.to_string());
               json.set("created_at", created);
               json.set("founded_at",league.founded_at.format("%Y-%m-%d").to_string());
            });
            client.json(&packet.unwrap())
        })
    }

    pub fn get_conference<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.desc("Get conferences for a league");

        endpoint.params(|params| {
            params.opt(response::LIMIT, |lmt| {
                lmt.coerce(json_dsl::i64());
                lmt.desc("Number of results to return");
                lmt.default(25)
            });
            
            params.opt(response::OFFSET, |offset| {
                offset.desc("The number of records to return");
                offset.default(0);
                offset.coerce(json_dsl::i64())
            });

            params.opt("year", |year| {
                year.desc("Restict conferences to a specific year");
                year.coerce(json_dsl::i64());
            })
        });
    
        endpoint.handle(|client, params| {
            let conn      = client.app.db();
            let year      = params.pointer("/year");
            let limit     = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
            let offset    = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
            let league_id = Uuid::parse_str(params.pointer("/id")
                                    .unwrap().as_str().unwrap()).unwrap();
            
            let results = League::conference_by_league(
                &*conn, &league_id, &limit, &offset, year
            );

            let packet = jsonway::object(|json| {
                json.object(response::META, |meta|{
                    let total = results.1 as i64;
                    let n = pagination::next(PREFIX, &limit, &offset, &total );
                    let p = pagination::prev(PREFIX, &limit, &offset, &total );

                    meta.set(response::COUNT, total);
                    meta.set(response::LIMIT, limit);
                    meta.set(response::URI, PREFIX);
                    meta.set(response::NEXT, n);
                    meta.set(response::PREVIOUS, p);
                });
                json.array(response::DATA, |data| {
                    data.objects(&mut results.0.iter(), |conference, obj| {
                        obj.set("league_conference_id", &conference.league_conference_id);
                        obj.set("name", &conference.name);
                        obj.set("created_at", time::at_utc(conference.created_at.clone()).rfc3339().to_string());
                    })
                })
            });
            client.json(&packet.unwrap())
        })
    }
}
