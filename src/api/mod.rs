extern crate rustless;
use rustless::batteries::swagger;
use errors;
use rustless::prelude::*;
use rustless::server::status;
use rustless::backend::Response;
mod game;
mod user;
mod team;
mod league;

pub fn root () -> rustless::Api {
    rustless::Api::build(|api|{
        api.prefix("api");
        api.version("v1", rustless::Versioning::Path);
        api.error_formatter(|err, _media| {
            if err.is::<errors::UnauthorizedError>() {
                return Some(Response::new(status::StatusCode::Unauthorized))
            } else if err.is::<errors::NotFoundError>() {
                return Some(Response::new(status::StatusCode::NotFound))
            }
            

            None
        });
        api.mount(game::api("game"));
        api.mount(user::api("user"));
        api.mount(team::api("team"));
        api.mount(league::api("league"));
        api.mount(swagger::create_api("api-docs"))
    })
}


pub mod pagination {

    pub fn next<'a>(pre: &'a str, limit: &i64, offset: &i64, total: &i64 ) -> Option<String> {
        let next =  if offset >= total { 0 } else { offset + limit };

        if next < *total {
            Some(format!(
                "{prefix}?limit={limit}&offset={offset}"
              , prefix=pre
              , limit=limit
              , offset=next
            ))
        } else {
            None
        }

    }

    pub fn prev<'a>(pre: &'a str, limit: &i64, offset: &i64, total: &i64 ) -> Option<String> {
        let previous = if *offset == 0 { 0i64 } else { offset - limit };
        if previous > 0i64 {
            Some(format!(
                "{prefix}?limit={limit}&offset={offset}"
              , prefix=pre
              , limit=limit
              , offset=previous
            ))
        } else {
            None
        }

    }

}
