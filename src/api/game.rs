extern crate uuid;
extern crate rustless;
extern crate jsonway;
extern crate time;
use rustless::framework::endpoint::EndpointHandlerPresent;
use rustless::{Endpoint, Nesting};
use db::models::Game;
use db::DatabaseExt;

const META: &'static str = "meta";
const NEXT: &'static str = "next";
const LIMIT: &'static str = "limit";
const COUNT: &'static str = "count";
const PREVIOUS: &'static str = "previous";

pub fn api(path: &str) -> rustless::Namespace {
    rustless::Namespace::build(path, |games| {
       games.get("", get_list )
    })
}


fn get_list<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
    endpoint.handle(|client, params| {
        let cn     = client.app.db();
        let games  = Game::latest(&*cn);
        let packet = jsonway::object(|json| {
            json.object(META, |meta| {
                meta.set(NEXT, "/api/v1/game?limit=25&offset=50"); 
                meta.set(PREVIOUS, "/api/v1/game?limit=25&offset=25");
                meta.set(COUNT, 100);
                meta.set(LIMIT, 25);
            });
            json.array("data", |data| {
                data.objects(&mut games.iter(), |game, obj| {                
                    obj.set("id", game.league_game_id.to_string());
                    obj.set("home_team", game.home_team.to_string());
                    obj.set("away_team", game.away_team.to_string());
                    obj.set("score", game.score);
                    obj.set("created_at", time::at_utc(game.created_at.clone()).rfc3339().to_string())
                })
            });
        });

        client.json(&packet.unwrap())
    })
}
