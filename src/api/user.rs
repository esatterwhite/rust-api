extern crate rustless;

use rustless::{Nesting,Namespace};

pub fn api(path: &str) -> Namespace {
    Namespace::build(path, |user| {
        user.get("", routes::get_list);
        user.get(":id", routes::get_detail);
        user.get(":id/teams", routes::get_teams)
    })
}

mod routes {
    extern crate jsonway;
    extern crate rustless;
    extern crate time;
    extern crate valico;
    extern crate uuid;

	use api::pagination;
    use constants::response;
    use self::valico::json_dsl;
    use self::uuid::Uuid;
    use rustless::Endpoint;
    use rustless::framework::endpoint::EndpointHandlerPresent;
    use db::models::{User, Team};
    use db::DatabaseExt;

    const PREFIX: &'static str = "/api/v1/user";
    pub fn get_list<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.desc("List all users");

        endpoint.params(|params| {
            params.opt(response::LIMIT, |lmt| {
                lmt.coerce(json_dsl::i64());
                lmt.desc("Number of results to return");
                lmt.default(25)
            });

            params.opt(response::OFFSET, |offset| {
                offset.desc("The number of records to skip");
                offset.default(0);
                offset.coerce(json_dsl::i64())
            })
        });

        endpoint.handle(|client, params| {
            let conn   = client.app.db();
            let limit  = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
            let offset = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
            let results  = User::latest(&*conn, limit, offset);
            let packet = jsonway::object(|json| {
                json.object(response::META, |meta| {
                    let total = results.1 as i64;
                    let n = pagination::next(PREFIX, &limit, &offset, &total );
                    let p = pagination::prev(PREFIX, &limit, &offset, &total );
                    meta.set(response::NEXT, n); 
                    meta.set(response::PREVIOUS, p);
                    meta.set(response::COUNT, results.1);
                    meta.set(response::LIMIT, &limit);
                });

                json.array(response::DATA, |data| {
                    data.objects(&mut results.0.iter(), |user, obj| {
                        obj.set("auth_user_id", user.auth_user_id.to_string());
                        obj.set("first_name", user.first_name.to_string());
                        obj.set("last_name", user.last_name.to_string());
                        obj.set("created_at", time::at_utc(user.created_at.clone()).rfc3339().to_string());
                    });
                });
            });
            client.json(&packet.unwrap())
        })
    }
    
    pub fn get_teams<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.params(|params| {
            params.opt(response::LIMIT, |lmt| {
                lmt.coerce(json_dsl::u64());
                lmt.desc("Number of results to return");
                lmt.default(25)
            });

            params.opt(response::OFFSET, |offset| {
                offset.desc("The number of records to skip");
                offset.default(0);
                offset.coerce(json_dsl::u64())
            })
        });

        endpoint.handle(|client, params| {
            let conn          = client.app.db();
            let user_id: Uuid = Uuid::parse_str(params.pointer("/id").unwrap().as_str().unwrap()).unwrap();
            let limit         = params.pointer(response::LIMIT_PTR).unwrap().as_i64().unwrap();
            let offset        = params.pointer(response::OFFSET_PTR).unwrap().as_i64().unwrap();
            let results       = User::teams(&*conn, user_id, limit, offset );
            let URI           = format!("/api/v1/user/{user_id}", user_id=user_id);

            let packet = jsonway::object(|json| {
                json.object(response::META, |meta| {
					let total = results.1 as i64;
                    let n = pagination::next(URI.as_str(), &limit, &offset, &total );
					let p = pagination::prev(URI.as_str(), &limit, &offset, &total );
                    
                    meta.set(response::COUNT, results.1);
                    meta.set(response::LIMIT, limit);
                    meta.set(response::URI, "/api/v1/user/{auth_user_id}/teams");
                    meta.set(response::NEXT, n);
					meta.set(response::PREVIOUS, p);
                });

                json.array(response::DATA, |data|{
                    data.objects(&mut results.0.iter(), |team, obj| {
                        obj.set("year", team.year);
                        obj.set("first_name", team.first_name.to_string());
                        obj.set("last_name", team.last_name.to_string());
                        obj.set("team", team.team.to_string());
                        obj.set("city", team.city.to_string());
                        obj.set("nickname", team.nickname.to_string());
                        obj.set("position", team.position.to_string());
                        obj.set("league_team_id", team.league_team_id.to_string());
                        obj.set("auth_user_id ", team.auth_user_id.to_string());
                    })
                });
            });

            client.json(&packet.unwrap())
        }) 
    }
    
    pub fn get_detail<'r>(endpoint: &'r mut Endpoint) -> EndpointHandlerPresent {
        endpoint.handle(|client, params| {
            let conn = client.app.db();
            let user_id: Uuid = Uuid::parse_str(params.pointer("/id").unwrap().as_str().unwrap()).unwrap();
            let user  = User::get(&*conn, user_id);

            let response = jsonway::object(|obj| {
                obj.set("auth_user_id", user.auth_user_id.to_string());
                obj.set("first_name", user.first_name.to_string());
                obj.set("last_name", user.last_name.to_string());
                obj.set("created_at", time::at_utc(user.created_at.clone()).rfc3339().to_string());
            });
            client.json(&response.unwrap())
        })
    }
}
