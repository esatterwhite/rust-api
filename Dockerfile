FROM alpine:latest

ENV DB_USER=user
ENV DB_PASS=password
ENV DB_NAME=sportsball
ENV DB_HOST=localhost
ENV DB_PORT=5432

COPY target/x86_64-unknown-linux-musl/release/api /usr/local/bin 
COPY target/x86_64-unknown-linux-musl/release/migrate /usr/local/bin
COPY bin/db/seeds/* /etc/seeds/

CMD ["api"]
