#[macro_use]
extern crate log;
extern crate env_logger;
extern crate docopt;
extern crate postgres;
extern crate scan_dir;
extern crate rustc_serialize;

mod db;
use std::env;
use std::io::{Read, Write};
use docopt::Docopt;
use postgres::{Connection, TlsMode};
use postgres::error::ConnectError;
use db::{Migration, Migrator};
use db::migrations::create_user_table::CreateUsers;
use db::migrations::create_team_table::Team;
use db::migrations::create_game_table::Game;
use db::migrations::create_league_table::League;
use db::migrations::create_team_conference::TeamConference;

const USAGE: &'static str = "
example - run migrate

Usage:
  migrate up
  migrate down
  migrate generate <name>
  migrate (-h | --help)

Options:
  -h --help     generate this message
";

#[derive(Debug, RustcDecodable)]
struct Args {
    cmd_up: bool,
    cmd_down: bool,
    cmd_generate: bool,
    arg_name: String,
}


fn main () {
    let args: Args =
        Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|e| e.exit());
    env_logger::init().unwrap();

    let user = match env::var( "DB_USER" ) {
        Ok(val) => val,
        _ => String::from("admin")
    };
    
    let pass = match env::var( "DB_PASS" ) {
        Ok(val) => val,
        _ => String::from("abc123")
    };

    let name = match env::var( "DB_NAME" ) {
        Ok(val) => val,
        _ => String::from("application")
    };
    
    let host = match env::var( "DB_HOST" ) {
        Ok(val) => val,
        _ => String::from("localhost")
    };
    
    let port = match env::var( "DB_PORT" ) {
        Ok(val) => val,
        _ => String::from("5432")
    };

    let DATABASE_URL = match env::var( "DATABASE_URL" ) {
        Ok(val) => val,
        _ => String::new()
    };
    
    let conn_str = if DATABASE_URL.len() > 0 { 
        DATABASE_URL 
    } else { 
        format!(
          "postgres://{user}:{pass}@{host}:{port}/{name}"
          , user=user
          , pass=pass
          , host=host
          , name=name
          , port=port
        )
    };

    let conn = Connection::connect(conn_str,TlsMode::None).unwrap();

    if args.cmd_up {
        up( conn );
    } else if args.cmd_down {
        down( conn );
    } else if args.cmd_generate {
        generate( args.arg_name );
    }
}


fn up( conn: Connection){
    info!("migrating up");
    let users  = CreateUsers::new();
    let team   =  Team::new();
    let game   =  Game::new();
    let league = League::new();
    let team_conference = TeamConference::new();
    
    let mut migrations: Vec<Box<Migration>> = Vec::new();
    migrations.push( Box::new( users ) );
    migrations.push( Box::new( league ) );
    migrations.push( Box::new( team ) );
    migrations.push( Box::new( team_conference ) );
    migrations.push( Box::new( game ) );

    for mig in migrations {
        mig.up( &conn );
    }
}

fn down( conn: Connection){
    info!("migrating down");
    let users  = CreateUsers::new();
    let team   =  Team::new();
    let game   =  Game::new();
    let league = League::new();
    let team_conference = TeamConference::new();
    
    let mut migrations: Vec<Box<Migration>> = Vec::new();
    migrations.push( Box::new( game ) );
    migrations.push( Box::new( team_conference ) );
    migrations.push( Box::new( team ) );
    migrations.push( Box::new( league ) );
    migrations.push( Box::new( users ) );

    for mig in migrations {
        mig.down( &conn );
    }
}

fn generate(name: String ){
    info!("generating {}", name);
}
