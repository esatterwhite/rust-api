
INSERT INTO
  league_team_conference(league_conference_year_id, league_team_id) ( 
    SELECT 
      league_conference_year.league_conference_year_id, league_team.league_team_id
    FROM
      league_conference_year
    CROSS JOIN (
      SELECT 
        *
      FROM 
        league_team
      ORDER BY random()
      
    ) as league_team
    
    INNER JOIN
      league on league.league_id = league_conference_year.league_id

    INNER JOIN 
      league_conference on league_conference.league_conference_id = league_conference_year.league_conference_id
    
    order by random() 
    LIMIT 5000
)
