INSERT INTO
	league_team_player (auth_user_id, league_team_id, year, position) (

		SELECT 
			auth_user_id
		, league_team_id
		, 2000 + (random() * 9 + 1)::integer as year
		, ('{Wide Receiver,Running Back,Quarterback,Fullback,Linebacker,Cornerback,Safety,Guard,Defensive End,Center}'::text[])[ceil(random()*9)]
		
		FROM 
			auth_user
		CROSS JOIN (
			SELECT 
				* 
			FROM 
				league_team 
			ORDER BY random() 
			LIMIT 6 
		) as league_team
)
