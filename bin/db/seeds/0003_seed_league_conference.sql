INSERT INTO 
  league_conference (name) 
VALUES 
('Western'), ('Eastern'), ('Northern'), ('Central'), ('Southern'), ('American'), ('National');

INSERT INTO 
  league_conference_year (league_conference_id, league_id, year) (

  SELECT league_conference.league_conference_id, league.league_id, years.* from
    league_conference
  CROSS JOIN ( 
    select * from league where league.name ='Northern Elite Football League'
  ) as league
  CROSS JOIN (
    select * from unnest(ARRAY[1990,1991,1992,1993,1994,1995,1996,1997,1998]) as year
  ) as years

  WHERE league_conference.name in ('Western', 'Eastern')
);

INSERT INTO 
  league_conference_year (league_conference_id, league_id, year) (

  SELECT league_conference.league_conference_id, league.league_id, years.* from
    league_conference
  CROSS JOIN ( 
    select * from league where league.name ='Ironman Football League'
  ) as league
  CROSS JOIN (
    select * from unnest(ARRAY[1998, 1999, 2000, 2001,2002,2003,2004,2005,2006,2008,2009,2010,2011,2012,2013,2014]) as year
  ) as years

  WHERE league_conference.name in ('American', 'National')
);

INSERT INTO 
  league_conference_year (league_conference_id, league_id, year) (

  SELECT league_conference.league_conference_id, league.league_id, years.* from
    league_conference
  CROSS JOIN ( 
    select * from league where league.name ='Southern States Football League'
  ) as league
  CROSS JOIN (
    select * from unnest(ARRAY[2004,2005,2006,2008,2009,2010,2011,2012,2013,2014,2015,2016]) as year
  ) as years

  WHERE league_conference.name in ('Northern', 'Central', 'Southern')
);
