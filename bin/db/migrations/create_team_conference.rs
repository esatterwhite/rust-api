extern crate log;
extern crate postgres;
use db::Migration;
use postgres::Connection;
use std::fmt::{self,Display};

pub struct TeamConference {
    name: &'static str,
    pub index: u8
}

impl TeamConference {
    pub fn new () -> Self {
        TeamConference {index: 5, name: "0005_create_team_conference"}
    }
}

impl Migration for TeamConference {

    fn up(&self, conn: &Connection) {
        info!("applying {}", self.name);
        let t = conn.transaction().unwrap();
        t.execute("
        CREATE TABLE public.league_team_conference (
          league_team_conference_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , league_team_id UUID REFERENCES league_team(league_team_id) NOT NULL
        , league_conference_year_id UUID REFERENCES league_conference_year(league_conference_year_id) NOT NULL
        )
        ", &[]).unwrap();
        t.set_commit();
        t.finish().unwrap();

        conn.execute("
        CREATE UNIQUE INDEX unq_team_conference_year on league_team_conference(league_team_id, league_conference_year_id)
        ",&[]).unwrap();
    
    }
    
    fn down(&self, conn: &Connection) {
        info!("reversing {}", self.name);
        let t = conn.transaction().unwrap();
        t.execute("
        DROP TABLE IF EXISTS public.league_team_conference;
        ", &[]).unwrap();
        t.set_commit();
        t.finish().unwrap();
    
    }
}

impl Display for TeamConference {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}
