extern crate log;
extern crate postgres;
use db::Migration;
use postgres::Connection;
use std::fmt::{self, Display};

pub struct Game {
    name: &'static str,
    pub index: u8
}

impl Game {
    pub fn new() -> Self {
        Game {index: 3, name: "0003_create_game_table"}
    }
}

impl Migration for Game {
    fn up(&self, conn: &Connection){
        info!("applying {}", self.name);
        let txn = conn.transaction().unwrap();
        txn.execute("
        CREATE TYPE game AS ENUM (
            'preseason', 'regular', 'playoff', 'championship', 'tournament'
        );
        ", &[]).unwrap();
        txn.set_commit();
        txn.finish();

        let txn = conn.transaction().unwrap();
        txn.execute("
        CREATE TABLE public.league_game(
          league_game_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()::timestamptz(3)
        , scheduled_at TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL
        , game_type game NOT NULL default 'regular' 
        , home_team UUID NOT NULL REFERENCES league_team (league_team_id)
        , away_team UUID NOT NULL REFERENCES league_team (league_team_id)
        , home_score INTEGER NULL DEFAULT NULL CHECK (home_score >= 0)
        , away_score INTEGER NULL DEFAULT NULL CHECK (away_score >= 0)
        );
        ", &[]).unwrap();
        txn.set_commit();
        txn.finish().unwrap();

        let txn2 = conn.transaction().unwrap();
        txn2.execute("
          CREATE INDEX on public.league_game (scheduled_at DESC);
        ", &[]).unwrap();
        txn2.set_commit();
        txn2.finish().unwrap();

        let txn2 = conn.transaction().unwrap();
        txn2.execute("
          CREATE INDEX on public.league_game (home_team);
        ", &[]).unwrap();
        txn2.set_commit();
        txn2.finish().unwrap();
        
        let txn2 = conn.transaction().unwrap();
        txn2.execute("
          CREATE INDEX on public.league_game (away_team);
        ", &[]).unwrap();
        txn2.set_commit();
        txn2.finish().unwrap();
        
        let txn2 = conn.transaction().unwrap();
        txn2.execute("
          CREATE UNIQUE INDEX on public.league_game (away_team, home_team, scheduled_at);
        ", &[]).unwrap();
        txn2.set_commit();
        txn2.finish().unwrap();
    }
    
    fn down(&self, conn: &Connection){
        info!("reversing {}", self.name);

        let txn = conn.transaction().unwrap();
        txn.execute("
        DROP TABLE IF EXISTS public.league_game
        ", &[]).unwrap();
        txn.set_commit();
        txn.finish().unwrap();
        
        let txn = conn.transaction().unwrap();
        txn.execute("
        DROP TYPE IF EXISTS game
        ", &[]).unwrap();
        txn.set_commit();
        txn.finish().unwrap();
    }
}

impl Display for Game {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}
