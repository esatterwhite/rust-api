extern crate log;
extern crate postgres;
use db::Migration;
use postgres::Connection;
use std::fmt::{self, Display};

pub struct CreateUsers {
    name: &'static str,
    pub index: i32,
}

impl CreateUsers {
    pub fn new() -> Self {
        CreateUsers {
            index:1,
            name: "0001_create_user_table",
        }
    }
}

impl Migration for CreateUsers {
    fn up(&self, conn: &Connection){
        let t = conn.transaction().unwrap();
        info!("applying {}", self.name);
        t.execute( "CREATE TABLE public.auth_user (
            auth_user_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
          , first_name VARCHAR(225) NOT NULL
          , last_name VARCHAR(225) NOT NULL
          , created_at TIMESTAMP WITH TIME ZONE DEFAULT now()::timestamptz(3)
          , birthdate DATE NULL
          , password TEXT
          );", &[]
        ).unwrap();

        t.set_commit();
        t.finish().unwrap();
    }

    fn down(&self, conn: &Connection){
        info!("reversing {}", self.name);
        conn.execute("DROP TABLE IF EXISTS auth_user", &[]).unwrap();
    }
}

impl Display for CreateUsers {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}
