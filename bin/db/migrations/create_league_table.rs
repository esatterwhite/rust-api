extern crate log;
extern crate postgres;
use db::Migration;
use postgres::Connection;
use std::fmt::{self,Display};

pub struct League {
    name: &'static str,
    pub index: u8
}

impl League {
    pub fn new () -> Self {
        League {index: 4, name: "0004_create_league_table"}
    }
}

impl Migration for League {
    fn up(&self, conn: &Connection){
        let txn = conn.transaction().unwrap();
        info!("applying {}", self.name);
        txn.execute("
        CREATE TABLE public.league(
          league_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , name VARCHAR(255) NOT NULL
        , created_at TIMESTAMP WITH TIME ZONE DEFAULT now()::timestamptz(3) NOT NULL
        , founded_at DATE NOT NULL
        );
        ", &[]
        ).unwrap();
        txn.set_commit();
        txn.finish().unwrap();

        let txn = conn.transaction().unwrap();
        txn.execute("
        CREATE TABLE public.league_conference(
          league_conference_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , name VARCHAR(75) NOT NULL
        , created_at TIMESTAMP WITH TIME ZONE DEFAULT now()::timestamptz(3) NOT NULL
        );
        ", &[]
        ).unwrap();
        txn.set_commit();
        txn.finish().unwrap();

        let txn = conn.transaction().unwrap();
        txn.execute("
        CREATE TABLE public.league_conference_year(
          league_conference_year_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , created_at TIMESTAMP WITH TIME ZONE DEFAULT now()::timestamptz(3) NOT NULL
        , league_id UUID NOT NULL REFERENCES league(league_id)
        , league_conference_id UUID NOT NULL REFERENCES league_conference(league_conference_id)
        , year INTEGER NOT NULL DEFAULT 1980 CHECK( year >= 1900 and year <= 3999)
        );
        ", &[]
        ).unwrap();
        txn.set_commit();
        txn.finish().unwrap();

       let idx = conn.transaction().unwrap();
       idx.execute("
        CREATE UNIQUE INDEX league_conference_year_idx on league_conference_year(league_id,year);               
        ", &[]).unwrap();
       idx.set_commit();
       idx.finish().unwrap();
       
       let idx = conn.transaction().unwrap();
       idx.execute("
        CREATE INDEX league_conference_id on league_conference_year(league_conference_id);              
        ", &[]).unwrap();
       idx.set_commit();
       idx.finish().unwrap();
       
       let idx = conn.transaction().unwrap();
       idx.execute("
        CREATE INDEX league_idx on league_conference_year(league_id);              
        ", &[]).unwrap();
       idx.set_commit();
       idx.finish().unwrap();
    }

    fn down(&self, conn: &Connection){
        let txn = conn.transaction().unwrap();
        info!("reversing {}", self.name);
        txn.execute("DROP TABLE IF EXISTS league_conference_year",&[]).unwrap();
        txn.set_commit();
        txn.finish();

        let txn = conn.transaction().unwrap();
        txn.execute("DROP TABLE IF EXISTS league_conference",&[]).unwrap();
        txn.set_commit();
        txn.finish();
        
        let txn = conn.transaction().unwrap();
        txn.execute("DROP TABLE IF EXISTS league",&[]).unwrap();
        txn.set_commit();
        txn.finish();
    }
}

impl Display for League {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}
