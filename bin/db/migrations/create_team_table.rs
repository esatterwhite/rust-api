extern crate log;
extern crate postgres;
use db::Migration;
use postgres::Connection;
use std::fmt::{self,Display};

pub struct Team {
    name: &'static str,
    pub index: u8
}

impl Team {
    pub fn new () -> Self {
        Team {index: 2, name: "0002_create_team_table"}
    }
}

impl Migration for Team {
    fn up(&self, conn: &Connection) {
       let t = conn.transaction().unwrap();
       info!("applying {}", self.name);

       t.execute("
        CREATE TABLE public.league_team(
          league_team_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
        , name VARCHAR(225) NOT NULL
        , created_at TIMESTAMP WITH TIME ZONE DEFAULT now()::timestamptz(3)
        , city VARCHAR(255) NOT NULL
        , nickname VARCHAR(3) NOT NULL
        , state VARCHAR(2) NOT NULL
        )
       ", &[]
       ).unwrap();
       t.set_commit();
       t.finish().unwrap();

       // staff
       let staff = conn.transaction().unwrap();
       staff.execute("
          CREATE TABLE public.league_team_staff(
            league_team_staff_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
          , league_team_id UUID NOT NULL REFERENCES league_team (league_team_id) 
          , auth_user_id UUID NOT NULL REFERENCES auth_user (auth_user_id)
          , role VARCHAR(50) NOT NULL DEFAULT 'head coach'
          , year integer NOT NULL DEFAULT 1980 CHECK( year >= 1900 AND year <= 3999)
        );", &[]).unwrap();
       staff.set_commit();
       staff.finish().unwrap();
        
       let staffidx = conn.transaction().unwrap();
       staffidx.execute("
        CREATE INDEX league_staff_year_idx on league_team_staff(year)               
        ", &[]).unwrap();
       staffidx.set_commit();
       staffidx.finish().unwrap();
       
       let staffidx = conn.transaction().unwrap();
       staffidx.execute("
        CREATE INDEX league_staff_user_idx on league_team_staff(auth_user_id) 
        ", &[]).unwrap();
       staffidx.set_commit();
       staffidx.finish().unwrap();

       let staffidx = conn.transaction().unwrap();
       staffidx.execute("
        CREATE INDEX league_staff_league_id_idx on league_team_staff(league_team_id)
        ", &[]).unwrap();
       staffidx.set_commit();
       staffidx.finish().unwrap();

       // players
       let player = conn.transaction().unwrap();
       player.execute("
          CREATE TABLE public.league_team_player(
            league_team_player_id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY
          , league_team_id UUID NOT NULL REFERENCES league_team (league_team_id) ON DELETE CASCADE
          , auth_user_id UUID NOT NULL REFERENCES auth_user (auth_user_id) ON DELETE CASCADE
          , position VARCHAR(20) NOT NULL DEFAULT 'quarterback'
          , year INTEGER NOT NULL DEFAULT 1980 CHECK( year >= 1900 AND year <= 3999)
        );", &[]).unwrap();
       player.set_commit();
       player.finish().unwrap();
        
       let playeridx = conn.transaction().unwrap();
       playeridx.execute("
        CREATE UNIQUE INDEX unq_league_player_year_idx on league_team_player(auth_user_id, league_team_id, year); 
        ", &[]).unwrap();
       playeridx.set_commit();
       playeridx.finish().unwrap();
       
       let playeridx = conn.transaction().unwrap();
       playeridx.execute("
        CREATE INDEX league_player_year_team_idx on league_team_player(year,league_team_id); 
        ", &[]).unwrap();
       playeridx.set_commit();
       playeridx.finish().unwrap();
       
       let playeridx = conn.transaction().unwrap();
       playeridx.execute("
        CREATE INDEX league_player_player_idx on league_team_player(auth_user_id); 
        ", &[]).unwrap();
       playeridx.set_commit();
       playeridx.finish().unwrap();
    }

    fn down(&self, conn: &Connection){
        info!("reversing {}", self.name);

        let t = conn.transaction().unwrap();
        t.execute("
          DROP TABLE IF EXISTS public.league_team_player;
        ", &[]).unwrap();
        t.set_commit();
        t.finish().unwrap();
        
        let t = conn.transaction().unwrap();
        t.execute("
          DROP TABLE IF EXISTS public.league_team_staff;
        ", &[]).unwrap();
        t.set_commit();
        t.finish().unwrap();

        let t = conn.transaction().unwrap();
        t.execute("
          DROP TABLE IF EXISTS public.league_team;
        ", &[]).unwrap();
        t.set_commit();
        t.finish().unwrap();


    }
}

impl Display for Team {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "{}", self.name)
    }
}
