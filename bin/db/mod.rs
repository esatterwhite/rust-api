extern crate typemap;
extern crate rustless;
extern crate postgres;

pub mod migrations;

use std::fmt::Display;
use self::rustless::Extensible;

/// Migration Trait
pub trait Migration: Display {
    fn up(&self, conn: &postgres::Connection);
    fn down(&self, conn: &postgres::Connection);
}


/// Migrator

pub struct Migrator<T> {
    migrations: Vec<Box<T>>,
}

impl<T> Migrator<T> {
    fn new(migrations: Vec<Box<T>> ) -> Self {
        Migrator { migrations: migrations }
    }

}
