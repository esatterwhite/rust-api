# Sports Ball API

Learning my way through [Rust](http://rust-lang.org) with this project. 
It is a fake sports / stats project with a JSON API.

Work in progress

## Building
Uses Stable rust 1.13
```bash
$ cargo run --bin migrate up
$ cargo run --bin api
```
* server running at `0.0.0.0:4000`
* api under `/api/v1`
* swagger docs `/api/v1/api-docs`